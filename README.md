# Exam Project : Investor Simulation

The aim of the project is to create a simulation of 3 types of investors: Aggressive, defensive and mixed.

## Step by Step instructions to get started:

### PART 1: [Deadline - 20.02.2021]
Model 2 types of bonds: Long Term and Short Term.

* Create a new repository on Bitbucket and make sure you have setup everything.
* Bonds have the following characteristics: a term that we’ll invest in them, a certain amount that will be invested, a minimum price of the bond, a minimum term and yearly interest rate.
* Short term Bonds: min term of 2 years, min amount of 250$ and yearly interest of 1.5%
* Long term Bonds: min term of 5 years, min amount of 1,000$ and yearly interest of 3%
* Both bonds are compounded yearly and the compounded interest for a certain time t can be calculated.
* Make a plot of the evolution of the investment of the minimum allowed invested amount for both bonds over a period of 50 years.

### PART 2: [Deadline - 27.02.2021]
Use Pandas to read stock data and model stocks. (Data available in csv for ‘FDX’, ‘GOOGL’, ‘XOM’, ‘KO’, ‘NOK’, ‘MS’, ‘IBM’ from 09.01.2016 to 01.01.2021.

* Stocks have the following criteria: A term during which investment will be done, a certain invested amount, a stockname, the number of stocks bought and the date on which they were bought (notice only business days are possible).
* We will assume that we can buy and sell them without additional transaction fees.
* Use the ‘High’ column as the price of the stocks.
* It should be possible to get the price of the stock and return on investment (given start and end date). If a start and end date is not a business day take the closest possible business day before the given date.
* Make a plot of all stocks for the entire period.

### PART 3: [Deadline - 10.03.2021]
Create 3 types of investors and set up everything so it works.

* An investor will start with a certain given budget, an investor mode [aggressive, defensive or mixed]
* A Defensive Investor: only invests in Short and long term bonds. He will randomly invest (50-50) in long and short term bonds as long as he has at least enough money to pay for a short term bond.
* An Aggressive Investor: only invest in stocks. First a randomly chosen stock will be selected. Then, depending on the investor's remaining budget, a random amount of stocks between 0 and the maximum of stocks that investor would be able to buy is bought. This is repeated until he has less than 100$ available.
* A mixed investor: invests with a 25-75% change in Bond or Stocks. If he buys a bond it is distributed 50-50 is long and short. If he buys a stock, the same rules apply as the aggressive investor. In this case the mixed investor will keep investing until he has not enough to buy the short term bond.
* Model 500 aggressive investors, 500 mixed and 500 defensive investors with a starting budget of 5000. What conclusions can you draw from this ?
For now you can assume bons are kept even after their minimum term.

### PART 4: [Deadline - 10.03.2021]
Test how the code can be used to model the following scenario on a separate fille Simulations.py to model the results.

* When taking the smae input parameters as above, and modelling the same number of investors with starting capital of 5000, plot the means for every year in the given dataset (always going from 01.01 to 31.12
* Now mixed investors redistribute their budget randomly again when their bond periods end (again 25-75 bonds vs. stocks, 50-50 long term short,...) what is the impact? What if switch 75-25 bonds vs stocks.
* Is there a change is all starting budgets are multiplied by 10?

####BONUS:

* What was on average the best year for an aggressive investor ?
* Model the same number of investors but now have starting budget randomly from a normal distribution centered around 20,000 with a standard deviation of 5000 (keep in mind the tails to the lower side).
* What was the best stock to have in 2007 ?



### Code Structure

## Comments:
Comments are to be written following a '#'.

## Instructions:
Instructions are to be written inbetween triple quotations as such: """ Instruction details """.

## Commits:
Every commit should have a short description of the improvements made.

## Tests:
Tests are to be commented out after they have been conducted. They should not be deleted in case of future bug to observe where future errors might have occured.


## Versioning

We used the persion 3.8 of Python.

## Authors

See also the list of [contributors](https://github.com/vanessa_nitteberg/examproject_investorsimulation/contributors) who participated in this project.
Vanessa Nitteberg
Cecile Dabin

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
