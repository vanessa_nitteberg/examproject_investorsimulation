from random import choice, uniform
from Part1_Bonds import ShortTermBonds, LongTermBonds
from Part2_Stocks import Stocks
from pandas.tseries.holiday import USFederalHolidayCalendar
from pandas.tseries.offsets import CustomBusinessDay
import os
import pandas as pd

# Import the csv files to the project
StockGoogle = os.path.abspath('../Data/GOOGL.csv')
StockFdx = os.path.abspath('../Data/FDX.csv')
StockIBM = os.path.abspath('../Data/IBM.csv')
StockKo = os.path.abspath('../Data/KO.csv')
StockMs = os.path.abspath('../Data/MS.csv')
StockNok = os.path.abspath('../Data/NOK.csv')
StockXom = os.path.abspath('../Data/XOM.csv')

# Separate Value by "," and only used column "high"
GOOGL = pd.read_csv(StockGoogle, sep=",", usecols=['high'])
FDX = pd.read_csv(StockFdx, sep=",", usecols=['high'])
IBM = pd.read_csv(StockIBM, sep=",", usecols=['high'])
KO = pd.read_csv(StockKo, sep=",", usecols=['high'])
MS = pd.read_csv(StockMs, sep=",", usecols=['high'])
NOK = pd.read_csv(StockNok, sep=",", usecols=['high'])
XOM = pd.read_csv(StockXom, sep=",", usecols=['high'])

"""Gives the dates but only excluding weekend so goes only up to 2020-11-05 which is off by almost 2 months"""
"""Gives the dates excluding week ends and US holidays, going up to 2021-01-08, only 7 days off"""
us_bd = CustomBusinessDay(calendar=USFederalHolidayCalendar())                      # Create Variable US business days
GOOGL['date'] = pd.bdate_range(start='9/1/2016', periods=len(GOOGL), freq=us_bd)   # Create column 'date' with us_bd
FDX['date'] = pd.bdate_range(start='9/1/2016', periods=len(FDX), freq=us_bd)
IBM['date'] = pd.bdate_range(start='9/1/2016', periods=len(IBM), freq=us_bd)
KO['date'] = pd.bdate_range(start='9/1/2016', periods=len(KO), freq=us_bd)
MS['date'] = pd.bdate_range(start='9/1/2016', periods=len(MS), freq=us_bd)
NOK['date'] = pd.bdate_range(start='9/1/2016', periods=len(NOK), freq=us_bd)
XOM['date'] = pd.bdate_range(start='9/1/2016', periods=len(XOM), freq=us_bd)


class Investor(object):                         # Created Investor Class
    def __init__(self, budget, date, term):     # With arguments Budget, date and term
        self.Budget = budget
        self.Date = date
        self.Term = term
        self.StartDate = self.Date


"""
An aggressive investor will only invest in stocks.
First a randomly chosen stock will be selected.
Then, depending on the investor remaining budget,
a random amount of stocks between 0 and the maximum of stocks that investor would be able to buy
is bought. This is repeated until he has less than 100$ available.
"""


class Aggressive(Investor):                     # Created Subclass Aggressive Investor
    def __init__(self, budget, date, term):     # With arguments Budget, date and term
        super().__init__(budget, date, term)

    def portfolio(self):                        # Portfolio function to calculate the return on investment
        randomstock = {"GOOGL": GOOGL, "FDX": FDX, "XOM": XOM, "KO": KO, "NOK": NOK, "MS": MS, "IBM": IBM}  # Key/value
        returnoninvestment = 0                  # Return on Investment variable starts at 0
        while self.Budget >= 100:               # While the budget is > 100 he continues to invest
            key = choice(list(randomstock))     # Random Stock is selected from the key/value pair variable
            randomamount = uniform(0, self.Budget)  # Random Amount to be invested is selected from available budget
            stock = Stocks(self.Term, randomstock[key], randomamount, self.Date)    # Stock variable with arguments
            self.Budget = self.Budget - randomamount                                # Budget reinitialised
            returnoninvestment = returnoninvestment + round(stock.return_on_investment(), 2)    # RoI updated
        return float(round(returnoninvestment, 4))                                  # Return final RoI at the end


# TEST AGGRESSIVE INVESTOR
TestAggressive = Aggressive(5000, "2016-09-01", 1505)
TestAggressive.portfolio()

"""A defensive investor will only invest in short and long term bonds.
He will randomly invest (50-50) in long and short term bonds
as long as he has at least enough money to pay for a short term bond."""


class Defensive(Investor):                      # Created Subclass Defensive Investor
    def __init__(self, budget, date, term):     # With arguments Budget, date and term
        super().__init__(budget, date, term)

    def portfolio(self):                        # Portfolio function to calculate return on investment
        returnoninvestment = 0.0                # return on investment set to 0
        while self.Budget > 250:                # While the budget is higher than 250 continue investing
            randombond = uniform(0, 1)          # A random number between 0 and 1
            if randombond <= 0.5:               # if lower than 0.5 invest in Short Term Bon
                randomamount = uniform(250, self.Budget)                                # Random amount to invest
                shorttermbondinvestment = ShortTermBonds(self.Term, randomamount)       # short term variable
                self.Budget = self.Budget - randomamount                                # available Budget updated
                returnoninvestment = returnoninvestment + shorttermbondinvestment.investment_return()   #RoI updated
            elif randombond > 0.5 and self.Budget >= 1000:                              # else if budget is high enough
                randomamount = uniform(1000, self.Budget)                               # Random amount to invest
                longtermbondinvestment = LongTermBonds(self.Term, randomamount)         # long term variable
                self.Budget = self.Budget - randomamount                                # Budget available updated
                returnoninvestment = returnoninvestment + longtermbondinvestment.investment_return()    #RoI updated
        return round(returnoninvestment, 4)                                             # Return final RoI at the end


# TEST DEFENSIVE INVESTOR:
TestDefensive = Defensive(5000, "2016-09-01", 1505)
TestDefensive.portfolio()

"""A mixed investor will invest first have a 25-75 chance of buying a bond or stocks.
If he buys a bond it is distributed 50-50 in long and short.
If he buys a stock, the same rules apply as the aggressive investor.
In this case the mixed investor will keep on investing until the has not enough
to buy the short term bond."""


class Mixed(Investor):                          # Created Subclass Mixed investor
    def __init__(self, budget, date, term):     # With arguments Budget, date and term
        super().__init__(budget, date, term)

    def portfolio(self):                        # Portfolio function to calculate return on investment
        returnoninvestment = 0.0
        while self.Budget > 250:
            randominvestment = uniform(0, 100)  # random number between 0 and 100
            if randominvestment <= 25:          # If lower than 25 invests in Bonds, same code as in Defensive
                randombond = uniform(0, 1)
                if randombond <= 0.5:
                    randomamount = uniform(250, self.Budget)
                    shorttermbondinvestment = ShortTermBonds(self.Term, randomamount)
                    self.Budget = self.Budget - randomamount
                    returnoninvestment = returnoninvestment + shorttermbondinvestment.investment_return()
                elif randombond > 0.5 and self.Budget >= 1000:
                    randomamount = uniform(1000, self.Budget)
                    longtermbondinvestment = LongTermBonds(self.Term, randomamount)
                    self.Budget = self.Budget - randomamount
                    returnoninvestment = returnoninvestment + longtermbondinvestment.investment_return()
            else:                               # Else he invests in Stocks, same code as in Aggressive
                randomstock = {"GOOGL": GOOGL, "FDX": FDX, "XOM": XOM, "KO": KO, "NOK": NOK, "MS": MS, "IBM": IBM}
                key = choice(list(randomstock))
                randomamount = uniform(0, self.Budget)
                stock = Stocks(self.Term, randomstock[key], randomamount, self.Date)
                self.Budget = self.Budget - randomamount
                returnoninvestment = returnoninvestment + round(stock.return_on_investment(), 2)
        return float(round(returnoninvestment, 4))


# TEST MIXED INVESTOR
TestMixed = Mixed(5000, "2016-09-01", 1505)
TestMixed.portfolio()

# Uncomment to run part 3, and comment if running Simulation of Bonus
"""
# SIMULATION:
# Model 500 aggressive investors,
print("500 aggressive investors: ")
AggressiveInvestment = []                                               # Created an empty list
for i in range(0, 500):                                                 # Create range from the 5 years to 50 years
    AggressiveSimulation = Aggressive(5000, "2016-09-01", 1505)         # Created variable for aggressive investor
    AggressiveInvestment.append(AggressiveSimulation.portfolio())       # Append to the list the Return on investment
print(AggressiveInvestment)                                             # Print the entire list
print("Average Return: ")
print(round(sum(AggressiveInvestment) / len(AggressiveInvestment), 2))  # print average return for all Investors

# 500 mixed investors
print("500 Mixed investors: ")
MixedInvestment = []
for i in range(0, 500):
    MixedSimulation = Mixed(5000, "2016-09-01", 1505)
    MixedInvestment.append(MixedSimulation.portfolio())
print(MixedInvestment)
print("Average Return: ")
print(round(sum(MixedInvestment) / len(MixedInvestment), 2))

# 500 defensive investors
print("500 Defensive investors: ")
DefensiveInvestment = []
for i in range(0, 500):
    DefensiveSimulation = Defensive(5000, "2016-09-01", 1505)
    DefensiveInvestment.append(DefensiveSimulation.portfolio())
print(DefensiveInvestment)
print("Average Return: ")
print(round(sum(DefensiveInvestment) / len(DefensiveInvestment), 2))"""

"""
# with a starting budget of 5000.
# What do you conclusions can you draw from this?

We can conclude that Aggressive investors are likely to earn much higher returns 
whilst mixed investors will earn the least, but Defensive investors earn far from 
as much as the aggressive types."""