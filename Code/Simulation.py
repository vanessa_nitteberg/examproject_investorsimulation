"""Test how the code can be used to model the following scenario
on a separate file Simulations.py to model the results.
"""

from Part3_Investors import Aggressive, Defensive, Mixed
from Part4_Code import NewMixed

"""
When taking the same input parameters as above,
and modelling the same number of investors with starting capital of 5000,
plot the means for every year in the given dataset (always going from 01.01 to 31.12)
"""

# SIMULATION:
# Model 500 aggressive investors,
print("///////////////////////////////////////////////////////")        # Devider to see clearer
print("500 aggressive investors: ")
AggressiveInvestment = []                                               # As in Part 3, empty lists are created
AggressiveInvestmentYear1 = []
AggressiveInvestmentYear2 = []
AggressiveInvestmentYear3 = []
AggressiveInvestmentYear4 = []
for i in range(0, 500):                                                 # Create range from the 0 to 500 investors
    AggressiveSimulation = Aggressive(5000, "2017-01-01", 1461)         # Model Aggressive investors for 4 year
    AggressiveInvestment.append(AggressiveSimulation.portfolio())       # Append to the list the returns
    AggressiveInvestorYear1 = Aggressive(5000, "2017-01-01", 365)
    AggressiveInvestmentYear1.append(AggressiveInvestorYear1.portfolio())
    AggressiveInvestorYear2 = Aggressive(5000, "2018-01-01", 365)
    AggressiveInvestmentYear2.append(AggressiveInvestorYear2.portfolio())
    AggressiveInvestorYear3 = Aggressive(5000, "2019-01-01", 365)
    AggressiveInvestmentYear3.append(AggressiveInvestorYear3.portfolio())
    AggressiveInvestorYear4 = Aggressive(5000, "2020-01-01", 366)
    AggressiveInvestmentYear4.append(AggressiveInvestorYear4.portfolio())
# print("All Agressive investors returns:", AggressiveInvestment)                                   # Print list
print("Total Average Return: ", round(sum(AggressiveInvestment) / len(AggressiveInvestment), 2))    # Print average RoI
print("_________________________________")                                                          # Devider
# print("All Agressive investors returns in 2017:", AggressiveInvestmentYear1)
print("Average Return in 2017: ", round(sum(AggressiveInvestmentYear1) / len(AggressiveInvestmentYear1), 2))
# print("All Agressive investors returns in 2018:", AggressiveInvestmentYear2)
print("Average Return in 2018: ", round(sum(AggressiveInvestmentYear2) / len(AggressiveInvestmentYear2), 2))
# print("All Agressive investors returns in 2019:", AggressiveInvestmentYear3)
print("Average Return in 2019: ", round(sum(AggressiveInvestmentYear3) / len(AggressiveInvestmentYear3), 2))
# print("All Agressive investors returns in 2020:", AggressiveInvestmentYear4)
print("Average Return in 2020: ", round(sum(AggressiveInvestmentYear4) / len(AggressiveInvestmentYear4), 2))

# 500 mixed investors
print("///////////////////////////////////////////////////////")
print("500 Mixed investors: ")
MixedInvestment = []
MixedInvestmentYear1 = []
MixedInvestmentYear2 = []
MixedInvestmentYear3 = []
MixedInvestmentYear4 = []
for i in range(0, 500):
    MixedSimulation = Mixed(5000, "2017-01-01", 1461)
    MixedInvestment.append(MixedSimulation.portfolio())
    MixedInvestorYear1 = Mixed(5000, "2017-01-01", 365)
    MixedInvestmentYear1.append(MixedInvestorYear1.portfolio())
    MixedInvestorYear2 = Mixed(5000, "2018-01-01", 365)
    MixedInvestmentYear2.append(MixedInvestorYear2.portfolio())
    MixedInvestorYear3 = Mixed(5000, "2019-01-01", 365)
    MixedInvestmentYear3.append(MixedInvestorYear3.portfolio())
    MixedInvestorYear4 = Mixed(5000, "2020-01-01", 366)
    MixedInvestmentYear4.append(MixedInvestorYear4.portfolio())
# print("All Mixed investors returns:", MixedInvestment)
print("Total Average Return: ", round(sum(MixedInvestment) / len(MixedInvestment), 2))
print("_________________________________")
# print("All Mixed investors returns in 2017:", MixedInvestmentYear1)
print("Average Return in 2017: ", round(sum(MixedInvestmentYear1) / len(MixedInvestmentYear1), 2))
# print("All Mixed investors returns in 2018:", MixedInvestmentYear2)
print("Average Return in 2018: ", round(sum(MixedInvestmentYear2) / len(MixedInvestmentYear2), 2))
# print("All Mixed investors returns in 2019:", MixedInvestmentYear3)
print("Average Return in 2019: ", round(sum(MixedInvestmentYear3) / len(MixedInvestmentYear3), 2))
# print("All Mixed investors returns in 2020:", MixedInvestmentYear4)
print("Average Return in 2020: ", round(sum(MixedInvestmentYear4) / len(MixedInvestmentYear4), 2))

# 500 defensive investors
print("///////////////////////////////////////////////////////")
print("500 Defensive investors: ")
DefensiveInvestment = []
DefensiveInvestmentYear1 = []
DefensiveInvestmentYear2 = []
DefensiveInvestmentYear3 = []
DefensiveInvestmentYear4 = []
for i in range(0, 500):
    DefensiveSimulation = Defensive(5000, "2017-01-01", 1461)
    DefensiveInvestment.append(DefensiveSimulation.portfolio())
    DefensiveInvestorYear1 = Defensive(5000, "2017-01-01", 365)
    DefensiveInvestmentYear1.append(DefensiveInvestorYear1.portfolio())
    DefensiveInvestorYear2 = Defensive(5000, "2018-01-01", 365)
    DefensiveInvestmentYear2.append(DefensiveInvestorYear2.portfolio())
    DefensiveInvestorYear3 = Defensive(5000, "2019-01-01", 365)
    DefensiveInvestmentYear3.append(DefensiveInvestorYear3.portfolio())
    DefensiveInvestorYear4 = Defensive(5000, "2020-01-01", 366)
    DefensiveInvestmentYear4.append(DefensiveInvestorYear4.portfolio())
# print("All Defensive investors returns:", DefensiveInvestment)
print("Total Average Return: ", round(sum(DefensiveInvestment) / len(DefensiveInvestment), 2))
print("_________________________________")
# print("All Defensive investors returns in 2017:", DefensiveInvestmentYear1)
print("Average Return in 2017: ", round(sum(DefensiveInvestmentYear1) / len(DefensiveInvestmentYear1), 2))
# print("All Defensive investors returns in 2018:", DefensiveInvestmentYear2)
print("Average Return in 2018: ", round(sum(DefensiveInvestmentYear2) / len(DefensiveInvestmentYear2), 2))
# print("All Defensive investors returns in 2019:", DefensiveInvestmentYear3)
print("Average Return in 2019: ", round(sum(DefensiveInvestmentYear3) / len(DefensiveInvestmentYear3), 2))
# print("All Defensive investors returns in 2020:", DefensiveInvestmentYear4)
print("Average Return in 2020: ", round(sum(DefensiveInvestmentYear4) / len(DefensiveInvestmentYear4), 2))


"""
Now mixed investors redistribute their budget randomly
again when their bond periods end (again 25-75 bonds vs. stocks, 50-50 long term short,...)
what is the impact? What if switch 75-25 bonds vs stocks.
"""

# To Change the code in Part 3 but not affect the existing code,
# We created a new file called Part4_Code to rewrite
# the existing code to adjust for the changes.

print("///////////////////////////////////////////////////////")
print("500 Mixed investors with strategy 25-75 bonds vs. stocks: ")
MixedInvestment = []
MixedInvestmentYear1 = []
MixedInvestmentYear2 = []
MixedInvestmentYear3 = []
MixedInvestmentYear4 = []
for i in range(0, 500):
    MixedSimulation = Mixed(5000, "2017-01-01", 1461)
    MixedInvestment.append(MixedSimulation.portfolio())
    MixedInvestorYear1 = Mixed(5000, "2017-01-01", 365)
    MixedInvestmentYear1.append(MixedInvestorYear1.portfolio())
    MixedInvestorYear2 = Mixed(5000, "2018-01-01", 365)
    MixedInvestmentYear2.append(MixedInvestorYear2.portfolio())
    MixedInvestorYear3 = Mixed(5000, "2019-01-01", 365)
    MixedInvestmentYear3.append(MixedInvestorYear3.portfolio())
    MixedInvestorYear4 = Mixed(5000, "2020-01-01", 366)
    MixedInvestmentYear4.append(MixedInvestorYear4.portfolio())
# print("All Mixed investors returns:", MixedInvestment)
print("Total Average Return: ", round(sum(MixedInvestment) / len(MixedInvestment), 2))
print("_________________________________")
# print("All Mixed investors returns in 2017:", MixedInvestmentYear1)
print("Average Return in 2017: ", round(sum(MixedInvestmentYear1) / len(MixedInvestmentYear1), 2))
# print("All Mixed investors returns in 2018:", MixedInvestmentYear2)
print("Average Return in 2018: ", round(sum(MixedInvestmentYear2) / len(MixedInvestmentYear2), 2))
# print("All Mixed investors returns in 2019:", MixedInvestmentYear3)
print("Average Return in 2019: ", round(sum(MixedInvestmentYear3) / len(MixedInvestmentYear3), 2))
# print("All Mixed investors returns in 2020:", MixedInvestmentYear4)
print("Average Return in 2020: ", round(sum(MixedInvestmentYear4) / len(MixedInvestmentYear4), 2))

print("///////////////////////////////////////////////////////")
print("500 Mixed investors with strategy 75-25 bonds vs. stocks: ")
NewMixedInvestment = []
NewMixedInvestmentYear1 = []
NewMixedInvestmentYear2 = []
NewMixedInvestmentYear3 = []
NewMixedInvestmentYear4 = []
for i in range(0, 500):
    NewMixedSimulation = NewMixed(5000, "2017-01-01", 1461)
    NewMixedInvestment.append(NewMixedSimulation.portfolio())
    NewMixedInvestorYear1 = NewMixed(5000, "2017-01-01", 365)
    NewMixedInvestmentYear1.append(NewMixedInvestorYear1.portfolio())
    NewMixedInvestorYear2 = NewMixed(5000, "2018-01-01", 365)
    NewMixedInvestmentYear2.append(NewMixedInvestorYear2.portfolio())
    NewMixedInvestorYear3 = NewMixed(5000, "2019-01-01", 365)
    NewMixedInvestmentYear3.append(NewMixedInvestorYear3.portfolio())
    NewMixedInvestorYear4 = NewMixed(5000, "2020-01-01", 366)
    NewMixedInvestmentYear4.append(NewMixedInvestorYear4.portfolio())
# print("All New Mixed investors returns:", NewMixedInvestment)
print("Total Average Return: ", round(sum(NewMixedInvestment) / len(NewMixedInvestment), 2))
print("_________________________________")
# print("All New Mixed investors returns in 2017:", NewMixedInvestmentYear1)
print("Average Return in 2017: ", round(sum(NewMixedInvestmentYear1) / len(NewMixedInvestmentYear1), 2))
# print("All New Mixed investors returns in 2018:", NewMixedInvestmentYear2)
print("Average Return in 2018: ", round(sum(NewMixedInvestmentYear2) / len(NewMixedInvestmentYear2), 2))
# print("All New Mixed investors returns in 2019:", NewMixedInvestmentYear3)
print("Average Return in 2019: ", round(sum(NewMixedInvestmentYear3) / len(NewMixedInvestmentYear3), 2))
# print("All New Mixed investors returns in 2020:", NewMixedInvestmentYear4)
print("Average Return in 2020: ", round(sum(NewMixedInvestmentYear4) / len(NewMixedInvestmentYear4), 2))

"""
Is there a change if all starting budgets are multiplied by 10?
"""

print("///////////////////////////////////////////////////////")
print("500 Mixed investors with strategy 25-75 bonds vs. stocks: ")
MixedInvestment = []
MixedInvestmentYear1 = []
MixedInvestmentYear2 = []
MixedInvestmentYear3 = []
MixedInvestmentYear4 = []
for i in range(0, 500):
    MixedSimulation = Mixed(50000, "2017-01-01", 1461)
    MixedInvestment.append(MixedSimulation.portfolio())
    MixedInvestorYear1 = Mixed(50000, "2017-01-01", 365)
    MixedInvestmentYear1.append(MixedInvestorYear1.portfolio())
    MixedInvestorYear2 = Mixed(50000, "2018-01-01", 365)
    MixedInvestmentYear2.append(MixedInvestorYear2.portfolio())
    MixedInvestorYear3 = Mixed(50000, "2019-01-01", 365)
    MixedInvestmentYear3.append(MixedInvestorYear3.portfolio())
    MixedInvestorYear4 = Mixed(50000, "2020-01-01", 366)
    MixedInvestmentYear4.append(MixedInvestorYear4.portfolio())
# print("All Mixed investors returns:", MixedInvestment)
print("Total Average Return: ", round(sum(MixedInvestment) / len(MixedInvestment), 2))
print("_________________________________")
# print("All Mixed investors returns in 2017:", MixedInvestmentYear1)
print("Average Return in 2017: ", round(sum(MixedInvestmentYear1) / len(MixedInvestmentYear1), 2))
# print("All Mixed investors returns in 2018:", MixedInvestmentYear2)
print("Average Return in 2018: ", round(sum(MixedInvestmentYear2) / len(MixedInvestmentYear2), 2))
# print("All Mixed investors returns in 2019:", MixedInvestmentYear3)
print("Average Return in 2019: ", round(sum(MixedInvestmentYear3) / len(MixedInvestmentYear3), 2))
# print("All Mixed investors returns in 2020:", MixedInvestmentYear4)
print("Average Return in 2020: ", round(sum(MixedInvestmentYear4) / len(MixedInvestmentYear4), 2))

print("///////////////////////////////////////////////////////")
print("500 Mixed investors with strategy 75-25 bonds vs. stocks: ")
NewMixedInvestment = []
NewMixedInvestmentYear1 = []
NewMixedInvestmentYear2 = []
NewMixedInvestmentYear3 = []
NewMixedInvestmentYear4 = []
for i in range(0, 500):
    NewMixedSimulation = NewMixed(50000, "2017-01-01", 1461)
    NewMixedInvestment.append(NewMixedSimulation.portfolio())
    NewMixedInvestorYear1 = NewMixed(50000, "2017-01-01", 365)
    NewMixedInvestmentYear1.append(NewMixedInvestorYear1.portfolio())
    NewMixedInvestorYear2 = NewMixed(50000, "2018-01-01", 365)
    NewMixedInvestmentYear2.append(NewMixedInvestorYear2.portfolio())
    NewMixedInvestorYear3 = NewMixed(50000, "2019-01-01", 365)
    NewMixedInvestmentYear3.append(NewMixedInvestorYear3.portfolio())
    NewMixedInvestorYear4 = NewMixed(50000, "2020-01-01", 366)
    NewMixedInvestmentYear4.append(NewMixedInvestorYear4.portfolio())
# print("All New Mixed investors returns:", NewMixedInvestment)
print("Total Average Return: ", round(sum(NewMixedInvestment) / len(NewMixedInvestment), 2))
print("_________________________________")
# print("All New Mixed investors returns in 2017:", NewMixedInvestmentYear1)
print("Average Return in 2017: ", round(sum(NewMixedInvestmentYear1) / len(NewMixedInvestmentYear1), 2))
# print("All New Mixed investors returns in 2018:", NewMixedInvestmentYear2)
print("Average Return in 2018: ", round(sum(NewMixedInvestmentYear2) / len(NewMixedInvestmentYear2), 2))
# print("All New Mixed investors returns in 2019:", NewMixedInvestmentYear3)
print("Average Return in 2019: ", round(sum(NewMixedInvestmentYear3) / len(NewMixedInvestmentYear3), 2))
# print("All New Mixed investors returns in 2020:", NewMixedInvestmentYear4)
print("Average Return in 2020: ", round(sum(NewMixedInvestmentYear4) / len(NewMixedInvestmentYear4), 2))
