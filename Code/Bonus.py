"""Model the same number of investors but now have starting budget
randomly from a normal distribution centered around 20,000 with a
standard deviation of 5000 (keep in mind the tails to the lower side)."""

import random
from random import choice, uniform
from Part1_Bonds import ShortTermBonds, LongTermBonds
from Part2_Stocks import Stocks
from pandas.tseries.holiday import USFederalHolidayCalendar
from pandas.tseries.offsets import CustomBusinessDay
import os
import pandas as pd

# Import the csv files to the project
StockGoogle = os.path.abspath('../Data/GOOGL.csv')
StockFdx = os.path.abspath('../Data/FDX.csv')
StockIBM = os.path.abspath('../Data/IBM.csv')
StockKo = os.path.abspath('../Data/KO.csv')
StockMs = os.path.abspath('../Data/MS.csv')
StockNok = os.path.abspath('../Data/NOK.csv')
StockXom = os.path.abspath('../Data/XOM.csv')

# Separate Value by "," and only used column "high"
GOOGL = pd.read_csv(StockGoogle, sep=",", usecols=['high'])
FDX = pd.read_csv(StockFdx, sep=",", usecols=['high'])
IBM = pd.read_csv(StockIBM, sep=",", usecols=['high'])
KO = pd.read_csv(StockKo, sep=",", usecols=['high'])
MS = pd.read_csv(StockMs, sep=",", usecols=['high'])
NOK = pd.read_csv(StockNok, sep=",", usecols=['high'])
XOM = pd.read_csv(StockXom, sep=",", usecols=['high'])

"""Gives the dates but only excluding weekend so goes only up to 2020-11-05 which is off by almost 2 months"""
"""Gives the dates excluding week ends and US holidays, going up to 2021-01-08, only 7 days off"""
us_bd = CustomBusinessDay(calendar=USFederalHolidayCalendar())                      # Create Variable US business days
GOOGL['date'] = pd.bdate_range(start='9/1/2016', periods=len(GOOGL), freq=us_bd)   # Create column 'date' with us_bd
FDX['date'] = pd.bdate_range(start='9/1/2016', periods=len(FDX), freq=us_bd)
IBM['date'] = pd.bdate_range(start='9/1/2016', periods=len(IBM), freq=us_bd)
KO['date'] = pd.bdate_range(start='9/1/2016', periods=len(KO), freq=us_bd)
MS['date'] = pd.bdate_range(start='9/1/2016', periods=len(MS), freq=us_bd)
NOK['date'] = pd.bdate_range(start='9/1/2016', periods=len(NOK), freq=us_bd)
XOM['date'] = pd.bdate_range(start='9/1/2016', periods=len(XOM), freq=us_bd)


class Investor(object):
    def __init__(self, date, term):
        self.Budget = random.normalvariate(20000, 5000)
        self.Date = date
        self.Term = term
        self.StartDate = self.Date


class Aggressive(Investor):
    def __init__(self, date, term):
        super().__init__(date, term)

    def portfolio(self):
        randomstock = {"GOOGL": GOOGL, "FDX": FDX, "XOM": XOM, "KO": KO, "NOK": NOK, "MS": MS, "IBM": IBM}
        returnoninvestment = 0
        while self.Budget >= 100:
            key = choice(list(randomstock))
            randomamount = uniform(0, self.Budget)
            stock = Stocks(self.Term, randomstock[key], randomamount, self.Date)
            self.Budget = self.Budget - randomamount
            returnoninvestment = returnoninvestment + round(stock.return_on_investment(), 2)
        return float(round(returnoninvestment, 4))


class Defensive(Investor):
    def __init__(self, date, term):
        super().__init__(date, term)

    def portfolio(self):
        returnoninvestment = 0.0
        while self.Budget > 250:
            randombond = uniform(0, 1)
            if randombond <= 0.5:
                randomamount = uniform(250, self.Budget)
                shorttermbondinvestment = ShortTermBonds(self.Term, randomamount)
                self.Budget = self.Budget - randomamount
                returnoninvestment = returnoninvestment + shorttermbondinvestment.investment_return()
            elif randombond > 0.5 and self.Budget >= 1000:
                randomamount = uniform(1000, self.Budget)
                longtermbondinvestment = LongTermBonds(self.Term, randomamount)
                self.Budget = self.Budget - randomamount
                returnoninvestment = returnoninvestment + longtermbondinvestment.investment_return()
        return round(returnoninvestment, 4)


class Mixed(Investor):
    def __init__(self, date, term):
        super().__init__(date, term)

    def portfolio(self):
        returnoninvestment = 0.0
        while self.Budget > 250:
            randominvestment = uniform(0, 100)
            if randominvestment <= 25:
                randombond = uniform(0, 1)
                if randombond <= 0.5:
                    randomamount = uniform(250, self.Budget)
                    shorttermbondinvestment = ShortTermBonds(self.Term, randomamount)
                    self.Budget = self.Budget - randomamount
                    returnoninvestment = returnoninvestment + shorttermbondinvestment.investment_return()
                elif randombond > 0.5 and self.Budget >= 1000:
                    randomamount = uniform(1000, self.Budget)
                    longtermbondinvestment = LongTermBonds(self.Term, randomamount)
                    self.Budget = self.Budget - randomamount
                    returnoninvestment = returnoninvestment + longtermbondinvestment.investment_return()
            else:
                randomstock = {"GOOGL": GOOGL, "FDX": FDX, "XOM": XOM, "KO": KO, "NOK": NOK, "MS": MS, "IBM": IBM}
                key = choice(list(randomstock))
                randomamount = uniform(0, self.Budget)
                stock = Stocks(self.Term, randomstock[key], randomamount, self.Date)
                self.Budget = self.Budget - randomamount
                returnoninvestment = returnoninvestment + round(stock.return_on_investment(), 2)
        return float(round(returnoninvestment, 4))


# SIMULATION: with random budget following a normal distribution:
# Centered around 20,000 and
# standard deviation of 5000

print("500 aggressive investors: ")
AggressiveInvestment = []
for i in range(0, 500):                                                 # Create range from the 5 years to 50 years
    AggressiveSimulation = Aggressive("2017-01-01", 1461)
    AggressiveInvestment.append(AggressiveSimulation.portfolio())
print(AggressiveInvestment)
print("Average Return: ")
print(round(sum(AggressiveInvestment) / len(AggressiveInvestment), 2))

print("500 Mixed investors: ")
MixedInvestment = []
for i in range(0, 500):                                                 # Create range from the 5 years to 50 years
    MixedSimulation = Mixed("2017-01-01", 1461)
    MixedInvestment.append(MixedSimulation.portfolio())
print(MixedInvestment)
print("Average Return: ")
print(round(sum(MixedInvestment) / len(MixedInvestment), 2))

print("500 Defensive investors: ")
DefensiveInvestment = []
for i in range(0, 500):                                                 # Create range from the 5 years to 50 years
    DefensiveSimulation = Defensive("2017-01-01", 1461)
    DefensiveInvestment.append(DefensiveSimulation.portfolio())
print(DefensiveInvestment)
print("Average Return: ")
print(round(sum(DefensiveInvestment) / len(DefensiveInvestment), 2))


"""What was the best stock to have in 2017 ?"""
print("Yearly return of a 1000$ investment in 2017:")
InvestmentGOOGL = Stocks(365, GOOGL, 1000, "2017-01-01")
print("GOOGL:", InvestmentGOOGL.return_on_investment())
InvestmentFDX = Stocks(365, FDX, 1000, "2017-01-01")
print("FDX:", InvestmentFDX.return_on_investment())
InvestmentIBM = Stocks(365, IBM, 1000, "2017-01-01")
print("IBM", InvestmentIBM.return_on_investment())
InvestmentKO = Stocks(365, KO, 1000, "2017-01-01")
print("KO", InvestmentKO.return_on_investment())
InvestmentMS = Stocks(365, MS, 1000, "2017-01-01")
print("MS", InvestmentMS.return_on_investment())
InvestmentNOK = Stocks(365, NOK, 1000, "2017-01-01")
print("NOK", InvestmentNOK.return_on_investment())
InvestmentXOM = Stocks(365, XOM, 1000, "2017-01-01")
print("XOM", InvestmentXOM.return_on_investment())

# We can see that the best stock to have in 2017 was Google at 31,41% return
# between the 1st of January and the 31st of December
