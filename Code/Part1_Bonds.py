"""
In this first part on Bonds, we assume that the coupon of the bond is 1,5%
for Short term Bonds and 3% for Long term Bonds, they are paid annually and reinvest.
We decided to use the formula :
Final amount = Initial price * (1+ Interest Rate)^number of period
"""
from matplotlib import pyplot as plt

"""
Bonds have the following characteristics: 
a term that we’ll invest in them, 
a certain amount that will be invested, 
a minimum price of the bond, 
a minimum term and yearly interest rate.
Both bonds are compounded yearly and the compounded interest for a certain time t can be calculated.
"""

# PART 1 USING CLASSES


class Bonds(object):                                # Create class for Bond
    def __init__(self, term, amount):               # Common arguments to bonds = term and Amount
        self.Term = term                            # Term in number of Days
        self.Amount = amount                        # Amount in Euros
        self.Year = self.Term / 365


"""Short term Bonds: min term of 2 years, min amount of 250$ and yearly interest of 1.5%"""


class ShortTermBonds(Bonds):                        # Create Subclass to bonds
    def __init__(self, term, amount):               # Assign the arguments of bonds to the subclass
        super().__init__(term, amount)
        self.Interest = 0.015                       # Assign a fixed value to the Interest variable so user cant change

    def investment_return(self):                     # Create function to calculate value of investment
        if self.Amount >= 250:  # and self.Year >= 2:
            return round(((self.Amount * (1 + self.Interest) ** self.Year) - self.Amount), 2)   # Value of investment


"""Long term Bonds: min term of 5 years, min amount of 1,000$ and yearly interest of 3%"""


class LongTermBonds(Bonds):                         # Create Subclass to bonds
    def __init__(self, term, amount):               # Assign the arguments of bonds to the subclass
        super().__init__(term, amount)
        self.Interest = 0.03                        # Assign a fixed value to the Interest variable so user cant change

    def investment_return(self):
        if self.Amount >= 1000:  # and self.Year >= 5:
            return round(((self.Amount * (1 + self.Interest) ** self.Year) - self.Amount), 2)   # Value of investment


# TEST THE CLASSES WORK
# SHORT TERM BOND
# Create a ST investment
ShortTermInvestmentTest = ShortTermBonds(1461, 250)
print(ShortTermInvestmentTest.investment_return())               # Test Function Investment Return
# LONG TERM BOND
LongTermInvestmentTest = LongTermBonds(1461, 1000)              # Create a LT investment
print(LongTermInvestmentTest.investment_return())                # Test Function Investment Return

"""
Make a plot of the evolution of the investment of the minimum allowed 
invested amount for both bonds over a period of 50 years.
"""

# PLOT THE EVOLUTION OF A 50 YEAR INVESTMENT:
# SHORT TERM BOND
Investment_Short_Term_Bond = []                        # Create empty list
for i in range(2, 50):                                 # Create range from the minimum 2 years to 50 years
    i = i * 365
    Short_Term_Investment = ShortTermBonds(i, 250)
    Investment_Short_Term_Bond.append(Short_Term_Investment.investment_return())  # Append to the list each year

plt.plot(Investment_Short_Term_Bond, label='Short Term Bond investment of 250$ for 50 years')
plt.xlabel("Years")                                    # Label the x axis
plt.ylabel("Investment Value")                         # Label the y axis
#plt.show()                                            # plt.show() to display the plot
print(Investment_Short_Term_Bond)

# LONG TERM BOND
Investment_Long_Term_Bond = []                         # Create empty list
for i in range(5, 50):                                 # Create range from the 5 years to 50 years
    i = i * 365
    Long_Term_Investment = LongTermBonds(i, 1000)
    Investment_Long_Term_Bond.append(Long_Term_Investment.investment_return())  # Append to list each investment year

plt.plot(Investment_Long_Term_Bond, label='Long Term Bond investment of 1000$ for 50 years')
plt.xlabel("Years")                                    # Label the x axis
plt.ylabel("Investment Value")                         # Label the y axis
#plt.show()                                             # plt.show() to display the plot
print(Investment_Long_Term_Bond)

# Graph of both Short Term and Long Term bonds
plt.plot(Investment_Short_Term_Bond, label='Short Term Bond')
plt.plot(Investment_Long_Term_Bond, label='Long Term Bond')
plt.xlabel('Years')
plt.ylabel('Investment Value')
plt.legend()
#plt.show()
