import pandas as pd
from pandas.tseries.holiday import USFederalHolidayCalendar
from pandas.tseries.offsets import CustomBusinessDay
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import os

"""
Use Pandas to read stock data and model stocks. (Data available in csv for ‘FDX’, ‘GOOGL’, ‘XOM’, ‘KO’, ‘NOK’, ‘MS’,
 ‘IBM’ from 09.01.2016 to 01.01.2021.
"""

# Import the csv files to the project
StockGoogle = os.path.abspath('../Data/GOOGL.csv')
StockFdx = os.path.abspath('../Data/FDX.csv')
StockIBM = os.path.abspath('../Data/IBM.csv')
StockKo = os.path.abspath('../Data/KO.csv')
StockMs = os.path.abspath('../Data/MS.csv')
StockNok = os.path.abspath('../Data/NOK.csv')
StockXom = os.path.abspath('../Data/XOM.csv')

# Separate Value by "," and only used column "high"
GOOGL = pd.read_csv(StockGoogle, sep=",", usecols=['high'])
FDX = pd.read_csv(StockFdx, sep=",", usecols=['high'])
IBM = pd.read_csv(StockIBM, sep=",", usecols=['high'])
KO = pd.read_csv(StockKo, sep=",", usecols=['high'])
MS = pd.read_csv(StockMs, sep=",", usecols=['high'])
NOK = pd.read_csv(StockNok, sep=",", usecols=['high'])
XOM = pd.read_csv(StockXom, sep=",", usecols=['high'])

"""Gives the dates excluding week ends and US holidays, going up to 2021-01-08, only 7 days off"""

us_bd = CustomBusinessDay(calendar=USFederalHolidayCalendar())                      # Create Variable US business days
GOOGL['date'] = pd.bdate_range(start='9/1/2016', periods=len(GOOGL), freq=us_bd)    # Create column 'date' with us_bd
# print(GOOGL)
FDX['date'] = pd.bdate_range(start='9/1/2016', periods=len(FDX), freq=us_bd)
# print(FDX)
IBM['date'] = pd.bdate_range(start='9/1/2016', periods=len(IBM), freq=us_bd)
# print(IBM)
KO['date'] = pd.bdate_range(start='9/1/2016', periods=len(KO), freq=us_bd)
# print(KO)
MS['date'] = pd.bdate_range(start='9/1/2016', periods=len(MS), freq=us_bd)
# print(MS)
NOK['date'] = pd.bdate_range(start='9/1/2016', periods=len(NOK), freq=us_bd)
# print(NOK)
XOM['date'] = pd.bdate_range(start='9/1/2016', periods=len(XOM), freq=us_bd)
# print(XOM)

"""Stocks have the following criteria: a term during which the investment will be done, a certain invested amount, 
a stock name (see above), the number of stocks bought and the date on which they we’re bought 
(notice only business days are possible)

We will assume that we can buy and sell them without additional transaction fees.
Use the ‘High’ column as the price of the stocks.
Make a plot of all stocks for the entire period.

It should be possible to get the price of the stock and return on investment (given start and end date). 
If a start and end date is not a business day take the closest possible business day before the given date.
"""

# Plot of all the Stocks together
plt.plot(GOOGL['date'], GOOGL['high'], label="GOOGL")
plt.plot(FDX['date'], FDX['high'], label="FDX")
plt.plot(IBM['date'], IBM['high'], label="IBM")
plt.plot(KO['date'], KO['high'], label="KO")
plt.plot(MS['date'], MS['high'], label="MS")
plt.plot(NOK['date'], NOK['high'], label="NOK")
plt.plot(XOM['date'], XOM['high'], label="XOM")
plt.xlabel('high price')
plt.ylabel('dates')
plt.title('Plot of all the stock prices over the period')
plt.legend()
#plt.show()

# PART 2 USING CLASSES


class Stocks:
    def __init__(self, term, ticker, amount, date):  # Common arguments = Name, term, Amount, number of stocks, and date
        self.Term = term
        self.Ticker = ticker
        self.Amount = amount
        self.Date = datetime.strptime(date, "%Y-%m-%d")
        self.EndDate = self.Date - timedelta(days=-self.Term)
        self.Number = float(round((self.Amount/(self.Ticker.loc[self.Ticker["date"] == self.start_date(), "high"])), 4))

    def start_date(self):
        while self.Date not in set(self.Ticker['date']):
            self.Date = self.Date - timedelta(days=1)
        else:
            self.Date = self.Date
            return self.Date

    def end_date(self):
        while self.EndDate not in set(self.Ticker['date']):
            self.EndDate = self.EndDate - timedelta(days=1)
        else:
            self.EndDate = self.EndDate
            return self.EndDate

    def price(self):
        return self.Ticker.loc[self.Ticker["date"] == self.start_date, "high"]

    def return_on_investment(self):
        startdate = self.start_date()
        enddate = self.end_date()
        pricebuy = float(self.Ticker.loc[self.Ticker["date"] == startdate, "high"]) * self.Number
        pricesell = float(self.Ticker.loc[self.Ticker["date"] == enddate, "high"]) * self.Number
        return round((pricesell - pricebuy), 4)


InvestmentXOM = Stocks(91, IBM, 1000, "2016-09-03")
print(InvestmentXOM.start_date())
print(InvestmentXOM.end_date())
print(InvestmentXOM.return_on_investment())
