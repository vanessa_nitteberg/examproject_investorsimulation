# File created to modify the Investor code for the below simulation instructions:

"""Now mixed investors redistribute their budget randomly
again when their bond periods end (again 25-75 bonds vs. stocks, 50-50 long term short,...)
what is the impact? What if switch 75-25 bonds vs stocks."""

from random import choice, uniform
from Part1_Bonds import ShortTermBonds, LongTermBonds
from Part2_Stocks import Stocks
from pandas.tseries.holiday import USFederalHolidayCalendar
from pandas.tseries.offsets import CustomBusinessDay
import os
import pandas as pd

# Import the csv files to the project
StockGoogle = os.path.abspath('../Data/GOOGL.csv')
StockFdx = os.path.abspath('../Data/FDX.csv')
StockIBM = os.path.abspath('../Data/IBM.csv')
StockKo = os.path.abspath('../Data/KO.csv')
StockMs = os.path.abspath('../Data/MS.csv')
StockNok = os.path.abspath('../Data/NOK.csv')
StockXom = os.path.abspath('../Data/XOM.csv')

# Separate Value by "," and only used column "high"
GOOGL = pd.read_csv(StockGoogle, sep=",", usecols=['high'])
FDX = pd.read_csv(StockFdx, sep=",", usecols=['high'])
IBM = pd.read_csv(StockIBM, sep=",", usecols=['high'])
KO = pd.read_csv(StockKo, sep=",", usecols=['high'])
MS = pd.read_csv(StockMs, sep=",", usecols=['high'])
NOK = pd.read_csv(StockNok, sep=",", usecols=['high'])
XOM = pd.read_csv(StockXom, sep=",", usecols=['high'])

"""Gives the dates but only excluding weekend so goes only up to 2020-11-05 which is off by almost 2 months"""
"""Gives the dates excluding week ends and US holidays, going up to 2021-01-08, only 7 days off"""

us_bd = CustomBusinessDay(calendar=USFederalHolidayCalendar())                      # Create Variable US business days
GOOGL['date'] = pd.bdate_range(start='9/1/2016', periods=len(GOOGL), freq=us_bd)   # Create column 'date' with us_bd
FDX['date'] = pd.bdate_range(start='9/1/2016', periods=len(FDX), freq=us_bd)
IBM['date'] = pd.bdate_range(start='9/1/2016', periods=len(IBM), freq=us_bd)
KO['date'] = pd.bdate_range(start='9/1/2016', periods=len(KO), freq=us_bd)
MS['date'] = pd.bdate_range(start='9/1/2016', periods=len(MS), freq=us_bd)
NOK['date'] = pd.bdate_range(start='9/1/2016', periods=len(NOK), freq=us_bd)
XOM['date'] = pd.bdate_range(start='9/1/2016', periods=len(XOM), freq=us_bd)


class NewInvestor(object):
    def __init__(self, budget, date, term):
        self.Budget = budget
        self.Date = date
        self.Term = term
        self.StartDate = self.Date

# Same Code as in Part 3 only the If changes from randominvestment <= 25 to randominvestment <= 75
class NewMixed(NewInvestor):
    def __init__(self, budget, date, term):
        super().__init__(budget, date, term)

    def portfolio(self):
        returnoninvestment = 0.0
        while self.Budget > 250:
            randominvestment = uniform(0, 100)
            if randominvestment <= 75:
                randombond = uniform(0, 1)
                if randombond <= 0.5:
                    randomamount = uniform(250, self.Budget)
                    shorttermbondinvestment = ShortTermBonds(self.Term, randomamount)
                    self.Budget = self.Budget - randomamount
                    returnoninvestment = returnoninvestment + shorttermbondinvestment.investment_return()
                elif randombond > 0.5 and self.Budget >= 1000:
                    randomamount = uniform(1000, self.Budget)
                    longtermbondinvestment = LongTermBonds(self.Term, randomamount)
                    self.Budget = self.Budget - randomamount
                    returnoninvestment = returnoninvestment + longtermbondinvestment.investment_return()
            else:
                randomstock = {"GOOGL": GOOGL, "FDX": FDX, "XOM": XOM, "KO": KO, "NOK": NOK, "MS": MS, "IBM": IBM}
                key = choice(list(randomstock))
                randomamount = uniform(0, self.Budget)
                stock = Stocks(self.Term, randomstock[key], randomamount, self.Date)
                self.Budget = self.Budget - randomamount
                returnoninvestment = returnoninvestment + round(stock.return_on_investment(), 2)
        return float(round(returnoninvestment, 4))
